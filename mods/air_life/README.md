# air_life

This mod adds many kinds of flying creatures, from birds to bats to fireflies.

## butterflies.lua
Authors of source code
----------------------
Shara RedCat (MIT)

Authors of media (textures)
---------------------------
Shara RedCat (CC BY-SA 3.0):
  butterflies_butterfly_*.png
  butterflies_butterfly_*_animated.png

##fireflies.lua



