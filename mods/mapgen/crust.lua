
--
-- Stone
--

minetest.register_node("default:stone", {
	description = S("Stone"),
	tiles = {"default_stone.png"},
	groups = {cracky = 3, stone = 1},
	drop = "default:cobble",
	legacy_mineral = true,
	sounds = default.node_sound_stone_defaults(),
})

minetest.register_node("default:cobble", {
	description = S("Cobblestone"),
	tiles = {"default_cobble.png"},
	is_ground_content = false,
	groups = {cracky = 3, stone = 2},
	sounds = default.node_sound_stone_defaults(),
})

minetest.register_node("default:stonebrick", {
	description = S("Stone Brick"),
	paramtype2 = "facedir",
	place_param2 = 0,
	tiles = {"default_stone_brick.png"},
	is_ground_content = false,
	groups = {cracky = 2, stone = 1},
	sounds = default.node_sound_stone_defaults(),
})

minetest.register_node("default:stone_block", {
	description = S("Stone Block"),
	tiles = {"default_stone_block.png"},
	is_ground_content = false,
	groups = {cracky = 2, stone = 1},
	sounds = default.node_sound_stone_defaults(),
})

minetest.register_node("default:mossycobble", {
	description = S("Mossy Cobblestone"),
	tiles = {"default_mossycobble.png"},
	is_ground_content = false,
	groups = {cracky = 3, stone = 1},
	sounds = default.node_sound_stone_defaults(),
})


minetest.register_node("default:desert_stone", {
	description = S("Desert Stone"),
	tiles = {"default_desert_stone.png"},
	groups = {cracky = 3, stone = 1},
	drop = "default:desert_cobble",
	legacy_mineral = true,
	sounds = default.node_sound_stone_defaults(),
})

minetest.register_node("default:desert_cobble", {
	description = S("Desert Cobblestone"),
	tiles = {"default_desert_cobble.png"},
	is_ground_content = false,
	groups = {cracky = 3, stone = 2},
	sounds = default.node_sound_stone_defaults(),
})

minetest.register_node("default:desert_stonebrick", {
	description = S("Desert Stone Brick"),
	paramtype2 = "facedir",
	place_param2 = 0,
	tiles = {"default_desert_stone_brick.png"},
	is_ground_content = false,
	groups = {cracky = 2, stone = 1},
	sounds = default.node_sound_stone_defaults(),
})

minetest.register_node("default:desert_stone_block", {
	description = S("Desert Stone Block"),
	tiles = {"default_desert_stone_block.png"},
	is_ground_content = false,
	groups = {cracky = 2, stone = 1},
	sounds = default.node_sound_stone_defaults(),
})

minetest.register_node("default:sandstone", {
	description = S("Sandstone"),
	tiles = {"default_sandstone.png"},
	groups = {crumbly = 1, cracky = 3},
	sounds = default.node_sound_stone_defaults(),
})

minetest.register_node("default:sandstonebrick", {
	description = S("Sandstone Brick"),
	paramtype2 = "facedir",
	place_param2 = 0,
	tiles = {"default_sandstone_brick.png"},
	is_ground_content = false,
	groups = {cracky = 2},
	sounds = default.node_sound_stone_defaults(),
})

minetest.register_node("default:sandstone_block", {
	description = S("Sandstone Block"),
	tiles = {"default_sandstone_block.png"},
	is_ground_content = false,
	groups = {cracky = 2},
	sounds = default.node_sound_stone_defaults(),
})

minetest.register_node("default:desert_sandstone", {
	description = S("Desert Sandstone"),
	tiles = {"default_desert_sandstone.png"},
	groups = {crumbly = 1, cracky = 3},
	sounds = default.node_sound_stone_defaults(),
})

minetest.register_node("default:desert_sandstone_brick", {
	description = S("Desert Sandstone Brick"),
	paramtype2 = "facedir",
	place_param2 = 0,
	tiles = {"default_desert_sandstone_brick.png"},
	is_ground_content = false,
	groups = {cracky = 2},
	sounds = default.node_sound_stone_defaults(),
})

minetest.register_node("default:desert_sandstone_block", {
	description = S("Desert Sandstone Block"),
	tiles = {"default_desert_sandstone_block.png"},
	is_ground_content = false,
	groups = {cracky = 2},
	sounds = default.node_sound_stone_defaults(),
})

minetest.register_node("default:silver_sandstone", {
	description = S("Silver Sandstone"),
	tiles = {"default_silver_sandstone.png"},
	groups = {crumbly = 1, cracky = 3},
	sounds = default.node_sound_stone_defaults(),
})

minetest.register_node("default:silver_sandstone_brick", {
	description = S("Silver Sandstone Brick"),
	paramtype2 = "facedir",
	place_param2 = 0,
	tiles = {"default_silver_sandstone_brick.png"},
	is_ground_content = false,
	groups = {cracky = 2},
	sounds = default.node_sound_stone_defaults(),
})

minetest.register_node("default:silver_sandstone_block", {
	description = S("Silver Sandstone Block"),
	tiles = {"default_silver_sandstone_block.png"},
	is_ground_content = false,
	groups = {cracky = 2},
	sounds = default.node_sound_stone_defaults(),
})

minetest.register_node("default:obsidian", {
	description = S("Obsidian"),
	tiles = {"default_obsidian.png"},
	sounds = default.node_sound_stone_defaults(),
	groups = {cracky = 1, level = 2},
})

minetest.register_node("default:obsidianbrick", {
	description = S("Obsidian Brick"),
	paramtype2 = "facedir",
	place_param2 = 0,
	tiles = {"default_obsidian_brick.png"},
	is_ground_content = false,
	sounds = default.node_sound_stone_defaults(),
	groups = {cracky = 1, level = 2},
})

minetest.register_node("default:obsidian_block", {
	description = S("Obsidian Block"),
	tiles = {"default_obsidian_block.png"},
	is_ground_content = false,
	sounds = default.node_sound_stone_defaults(),
	groups = {cracky = 1, level = 2},
})

--
-- Soft / Non-Stone
--

minetest.register_node("default:dirt", {
	description = S("Dirt"),
	tiles = {"default_dirt.png"},
	groups = {crumbly = 3, soil = 1},
	sounds = default.node_sound_dirt_defaults(),
})

minetest.register_node("default:dirt_with_grass", {
	description = S("Dirt with Grass"),
	tiles = {"default_grass.png", "default_dirt.png",
		{name = "default_dirt.png^default_grass_side.png",
			tileable_vertical = false}},
	groups = {crumbly = 3, soil = 1, spreading_dirt_type = 1},
	drop = "default:dirt",
	sounds = default.node_sound_dirt_defaults({
		footstep = {name = "default_grass_footstep", gain = 0.25},
	}),
})

minetest.register_node("default:dirt_with_grass_footsteps", {
	description = S("Dirt with Grass and Footsteps"),
	tiles = {"default_grass.png^default_footprint.png", "default_dirt.png",
		{name = "default_dirt.png^default_grass_side.png",
			tileable_vertical = false}},
	groups = {crumbly = 3, soil = 1, not_in_creative_inventory = 1},
	drop = "default:dirt",
	sounds = default.node_sound_dirt_defaults({
		footstep = {name = "default_grass_footstep", gain = 0.25},
	}),
})

minetest.register_node("default:dirt_with_dry_grass", {
	description = S("Dirt with Savanna Grass"),
	tiles = {"default_dry_grass.png",
		"default_dirt.png",
		{name = "default_dirt.png^default_dry_grass_side.png",
			tileable_vertical = false}},
	groups = {crumbly = 3, soil = 1, spreading_dirt_type = 1},
	drop = "default:dirt",
	sounds = default.node_sound_dirt_defaults({
		footstep = {name = "default_grass_footstep", gain = 0.4},
	}),
})

minetest.register_node("default:dirt_with_snow", {
	description = S("Dirt with Snow"),
	tiles = {"default_snow.png", "default_dirt.png",
		{name = "default_dirt.png^default_snow_side.png",
			tileable_vertical = false}},
	groups = {crumbly = 3, soil = 1, spreading_dirt_type = 1, snowy = 1},
	drop = "default:dirt",
	sounds = default.node_sound_dirt_defaults({
		footstep = {name = "default_snow_footstep", gain = 0.2},
	}),
})

minetest.register_node("default:dirt_with_rainforest_litter", {
	description = S("Dirt with Rainforest Litter"),
	tiles = {
		"default_rainforest_litter.png",
		"default_dirt.png",
		{name = "default_dirt.png^default_rainforest_litter_side.png",
			tileable_vertical = false}
	},
	groups = {crumbly = 3, soil = 1, spreading_dirt_type = 1},
	drop = "default:dirt",
	sounds = default.node_sound_dirt_defaults({
		footstep = {name = "default_grass_footstep", gain = 0.4},
	}),
})

minetest.register_node("default:dirt_with_coniferous_litter", {
	description = S("Dirt with Coniferous Litter"),
	tiles = {
		"default_coniferous_litter.png",
		"default_dirt.png",
		{name = "default_dirt.png^default_coniferous_litter_side.png",
			tileable_vertical = false}
	},
	groups = {crumbly = 3, soil = 1, spreading_dirt_type = 1},
	drop = "default:dirt",
	sounds = default.node_sound_dirt_defaults({
		footstep = {name = "default_grass_footstep", gain = 0.4},
	}),
})

minetest.register_node("default:dry_dirt", {
	description = S("Savanna Dirt"),
	tiles = {"default_dry_dirt.png"},
	groups = {crumbly = 3, soil = 1},
	sounds = default.node_sound_dirt_defaults(),
})

minetest.register_node("default:dry_dirt_with_dry_grass", {
	description = S("Savanna Dirt with Savanna Grass"),
	tiles = {"default_dry_grass.png", "default_dry_dirt.png",
		{name = "default_dry_dirt.png^default_dry_grass_side.png",
			tileable_vertical = false}},
	groups = {crumbly = 3, soil = 1},
	drop = "default:dry_dirt",
	sounds = default.node_sound_dirt_defaults({
		footstep = {name = "default_grass_footstep", gain = 0.4},
	}),
})

minetest.register_node("default:permafrost", {
	description = S("Permafrost"),
	tiles = {"default_permafrost.png"},
	groups = {cracky = 3},
	sounds = default.node_sound_dirt_defaults(),
})

minetest.register_node("default:permafrost_with_stones", {
	description = S("Permafrost with Stones"),
	tiles = {"default_permafrost.png^default_stones.png",
		"default_permafrost.png",
		"default_permafrost.png^default_stones_side.png"},
	groups = {cracky = 3},
	sounds = default.node_sound_gravel_defaults(),
})

minetest.register_node("default:permafrost_with_moss", {
	description = S("Permafrost with Moss"),
	tiles = {"default_moss.png", "default_permafrost.png",
		{name = "default_permafrost.png^default_moss_side.png",
			tileable_vertical = false}},
	groups = {cracky = 3},
	sounds = default.node_sound_dirt_defaults({
		footstep = {name = "default_grass_footstep", gain = 0.25},
	}),
})

minetest.register_node("default:sand", {
	description = S("Sand"),
	tiles = {"default_sand.png"},
	groups = {crumbly = 3, falling_node = 1, sand = 1},
	sounds = default.node_sound_sand_defaults(),
})

minetest.register_node("default:desert_sand", {
	description = S("Desert Sand"),
	tiles = {"default_desert_sand.png"},
	groups = {crumbly = 3, falling_node = 1, sand = 1},
	sounds = default.node_sound_sand_defaults(),
})

minetest.register_node("default:silver_sand", {
	description = S("Silver Sand"),
	tiles = {"default_silver_sand.png"},
	groups = {crumbly = 3, falling_node = 1, sand = 1},
	sounds = default.node_sound_sand_defaults(),
})


minetest.register_node("default:gravel", {
	description = S("Gravel"),
	tiles = {"default_gravel.png"},
	groups = {crumbly = 2, falling_node = 1},
	sounds = default.node_sound_gravel_defaults(),
	drop = {
		max_items = 1,
		items = {
			{items = {"default:flint"}, rarity = 16},
			{items = {"default:gravel"}}
		}
	}
})

minetest.register_node("default:clay", {
	description = S("Clay"),
	tiles = {"default_clay.png"},
	groups = {crumbly = 3},
	drop = "default:clay_lump 4",
	sounds = default.node_sound_dirt_defaults(),
})


minetest.register_node("default:snow", {
	description = S("Snow"),
	tiles = {"default_snow.png"},
	inventory_image = "default_snowball.png",
	wield_image = "default_snowball.png",
	paramtype = "light",
	buildable_to = true,
	floodable = true,
	drawtype = "nodebox",
	node_box = {
		type = "fixed",
		fixed = {
			{-0.5, -0.5, -0.5, 0.5, -0.25, 0.5},
		},
	},
	collision_box = {
		type = "fixed",
		fixed = {
			{-0.5, -0.5, -0.5, 0.5, -6 / 16, 0.5},
		},
	},
	groups = {crumbly = 3, falling_node = 1, snowy = 1},
	sounds = default.node_sound_snow_defaults(),

	on_construct = function(pos)
		pos.y = pos.y - 1
		if minetest.get_node(pos).name == "default:dirt_with_grass" then
			minetest.set_node(pos, {name = "default:dirt_with_snow"})
		end
	end,
})

minetest.register_node("default:snowblock", {
	description = S("Snow Block"),
	tiles = {"default_snow.png"},
	groups = {crumbly = 3, cools_lava = 1, snowy = 1},
	sounds = default.node_sound_snow_defaults(),

	on_construct = function(pos)
		pos.y = pos.y - 1
		if minetest.get_node(pos).name == "default:dirt_with_grass" then
			minetest.set_node(pos, {name = "default:dirt_with_snow"})
		end
	end,
})

-- 'is ground content = false' to avoid tunnels in sea ice or ice rivers
minetest.register_node("default:ice", {
	description = S("Ice"),
	tiles = {"default_ice.png"},
	is_ground_content = false,
	paramtype = "light",
	groups = {cracky = 3, cools_lava = 1, slippery = 3},
	sounds = default.node_sound_ice_defaults(),
})

-- Mapgen-placed ice with 'is ground content = true' to contain tunnels
minetest.register_node("default:cave_ice", {
	description = S("Cave Ice"),
	tiles = {"default_ice.png"},
	paramtype = "light",
	groups = {cracky = 3, cools_lava = 1, slippery = 3,
		not_in_creative_inventory = 1},
	drop = "default:ice",
	sounds = default.node_sound_ice_defaults(),
})


--
-- Aliases for map generators
--
minetest.register_alias("mapgen_stone", "mapgen:stone")
minetest.register_alias("mapgen_water_source", "mapgen:water_source")
minetest.register_alias("mapgen_river_water_source", "mapgen:river_water_source")

function default.register_biomes()

	-- Icesheet

	minetest.register_biome({
		name = "icesheet",
		node_dust = "mapgen:snowblock",
		node_top = "mapgen:snowblock",
		depth_top = 1,
		node_filler = "mapgen:snowblock",
		depth_filler = 3,
		node_stone = "mapgen:cave_ice",
		node_water_top = "mapgen:ice",
		depth_water_top = 10,
		node_river_water = "mapgen:ice",
		node_riverbed = "mapgen:gravel",
		depth_riverbed = 2,
		node_dungeon = "mapgen:ice",
		node_dungeon_stair = "stairs:stair_ice",
		y_max = 31000,
		y_min = -8,
		heat_point = 0,
		humidity_point = 73,
	})

	minetest.register_biome({
		name = "icesheet_ocean",
		node_dust = "mapgen:snowblock",
		node_top = "mapgen:sand",
		depth_top = 1,
		node_filler = "mapgen:sand",
		depth_filler = 3,
		node_water_top = "mapgen:ice",
		depth_water_top = 10,
		node_cave_liquid = "mapgen:water_source",
		node_dungeon = "mapgen:cobble",
		node_dungeon_alt = "mapgen:mossycobble",
		node_dungeon_stair = "stairs:stair_cobble",
		y_max = -9,
		y_min = -255,
		heat_point = 0,
		humidity_point = 73,
	})

	minetest.register_biome({
		name = "icesheet_under",
		node_cave_liquid = {"mapgen:water_source", "mapgen:lava_source"},
		node_dungeon = "mapgen:cobble",
		node_dungeon_alt = "mapgen:mossycobble",
		node_dungeon_stair = "stairs:stair_cobble",
		y_max = -256,
		y_min = -31000,
		heat_point = 0,
		humidity_point = 73,
	})

	-- Tundra

	minetest.register_biome({
		name = "tundra_highland",
		node_dust = "mapgen:snow",
		node_riverbed = "mapgen:gravel",
		depth_riverbed = 2,
		node_dungeon = "mapgen:cobble",
		node_dungeon_alt = "mapgen:mossycobble",
		node_dungeon_stair = "stairs:stair_cobble",
		y_max = 31000,
		y_min = 47,
		heat_point = 0,
		humidity_point = 40,
	})

	minetest.register_biome({
		name = "tundra",
		node_top = "mapgen:permafrost_with_stones",
		depth_top = 1,
		node_filler = "mapgen:permafrost",
		depth_filler = 1,
		node_riverbed = "mapgen:gravel",
		depth_riverbed = 2,
		node_dungeon = "mapgen:cobble",
		node_dungeon_alt = "mapgen:mossycobble",
		node_dungeon_stair = "stairs:stair_cobble",
		vertical_blend = 4,
		y_max = 46,
		y_min = 2,
		heat_point = 0,
		humidity_point = 40,
	})

	minetest.register_biome({
		name = "tundra_beach",
		node_top = "mapgen:gravel",
		depth_top = 1,
		node_filler = "mapgen:gravel",
		depth_filler = 2,
		node_riverbed = "mapgen:gravel",
		depth_riverbed = 2,
		node_dungeon = "mapgen:cobble",
		node_dungeon_alt = "mapgen:mossycobble",
		node_dungeon_stair = "stairs:stair_cobble",
		vertical_blend = 1,
		y_max = 1,
		y_min = -3,
		heat_point = 0,
		humidity_point = 40,
	})

	minetest.register_biome({
		name = "tundra_ocean",
		node_top = "mapgen:sand",
		depth_top = 1,
		node_filler = "mapgen:sand",
		depth_filler = 3,
		node_riverbed = "mapgen:gravel",
		depth_riverbed = 2,
		node_cave_liquid = "mapgen:water_source",
		node_dungeon = "mapgen:cobble",
		node_dungeon_alt = "mapgen:mossycobble",
		node_dungeon_stair = "stairs:stair_cobble",
		vertical_blend = 1,
		y_max = -4,
		y_min = -255,
		heat_point = 0,
		humidity_point = 40,
	})

	minetest.register_biome({
		name = "tundra_under",
		node_cave_liquid = {"mapgen:water_source", "mapgen:lava_source"},
		node_dungeon = "mapgen:cobble",
		node_dungeon_alt = "mapgen:mossycobble",
		node_dungeon_stair = "stairs:stair_cobble",
		y_max = -256,
		y_min = -31000,
		heat_point = 0,
		humidity_point = 40,
	})

	-- Taiga

	minetest.register_biome({
		name = "taiga",
		node_dust = "mapgen:snow",
		node_top = "mapgen:dirt_with_snow",
		depth_top = 1,
		node_filler = "mapgen:dirt",
		depth_filler = 3,
		node_riverbed = "mapgen:sand",
		depth_riverbed = 2,
		node_dungeon = "mapgen:cobble",
		node_dungeon_alt = "mapgen:mossycobble",
		node_dungeon_stair = "stairs:stair_cobble",
		y_max = 31000,
		y_min = 4,
		heat_point = 25,
		humidity_point = 70,
	})

	minetest.register_biome({
		name = "taiga_ocean",
		node_dust = "mapgen:snow",
		node_top = "mapgen:sand",
		depth_top = 1,
		node_filler = "mapgen:sand",
		depth_filler = 3,
		node_riverbed = "mapgen:sand",
		depth_riverbed = 2,
		node_cave_liquid = "mapgen:water_source",
		node_dungeon = "mapgen:cobble",
		node_dungeon_alt = "mapgen:mossycobble",
		node_dungeon_stair = "stairs:stair_cobble",
		vertical_blend = 1,
		y_max = 3,
		y_min = -255,
		heat_point = 25,
		humidity_point = 70,
	})

	minetest.register_biome({
		name = "taiga_under",
		node_cave_liquid = {"mapgen:water_source", "mapgen:lava_source"},
		node_dungeon = "mapgen:cobble",
		node_dungeon_alt = "mapgen:mossycobble",
		node_dungeon_stair = "stairs:stair_cobble",
		y_max = -256,
		y_min = -31000,
		heat_point = 25,
		humidity_point = 70,
	})

	-- Snowy grassland

	minetest.register_biome({
		name = "snowy_grassland",
		node_dust = "mapgen:snow",
		node_top = "mapgen:dirt_with_snow",
		depth_top = 1,
		node_filler = "mapgen:dirt",
		depth_filler = 1,
		node_riverbed = "mapgen:sand",
		depth_riverbed = 2,
		node_dungeon = "mapgen:cobble",
		node_dungeon_alt = "mapgen:mossycobble",
		node_dungeon_stair = "stairs:stair_cobble",
		y_max = 31000,
		y_min = 4,
		heat_point = 20,
		humidity_point = 35,
	})

	minetest.register_biome({
		name = "snowy_grassland_ocean",
		node_dust = "mapgen:snow",
		node_top = "mapgen:sand",
		depth_top = 1,
		node_filler = "mapgen:sand",
		depth_filler = 3,
		node_riverbed = "mapgen:sand",
		depth_riverbed = 2,
		node_cave_liquid = "mapgen:water_source",
		node_dungeon = "mapgen:cobble",
		node_dungeon_alt = "mapgen:mossycobble",
		node_dungeon_stair = "stairs:stair_cobble",
		vertical_blend = 1,
		y_max = 3,
		y_min = -255,
		heat_point = 20,
		humidity_point = 35,
	})

	minetest.register_biome({
		name = "snowy_grassland_under",
		node_cave_liquid = {"mapgen:water_source", "mapgen:lava_source"},
		node_dungeon = "mapgen:cobble",
		node_dungeon_alt = "mapgen:mossycobble",
		node_dungeon_stair = "stairs:stair_cobble",
		y_max = -256,
		y_min = -31000,
		heat_point = 20,
		humidity_point = 35,
	})

	-- Grassland

	minetest.register_biome({
		name = "grassland",
		node_top = "mapgen:dirt_with_grass",
		depth_top = 1,
		node_filler = "mapgen:dirt",
		depth_filler = 1,
		node_riverbed = "mapgen:sand",
		depth_riverbed = 2,
		node_dungeon = "mapgen:cobble",
		node_dungeon_alt = "mapgen:mossycobble",
		node_dungeon_stair = "stairs:stair_cobble",
		y_max = 31000,
		y_min = 6,
		heat_point = 50,
		humidity_point = 35,
	})

	minetest.register_biome({
		name = "grassland_dunes",
		node_top = "mapgen:sand",
		depth_top = 1,
		node_filler = "mapgen:sand",
		depth_filler = 2,
		node_riverbed = "mapgen:sand",
		depth_riverbed = 2,
		node_dungeon = "mapgen:cobble",
		node_dungeon_alt = "mapgen:mossycobble",
		node_dungeon_stair = "stairs:stair_cobble",
		vertical_blend = 1,
		y_max = 5,
		y_min = 4,
		heat_point = 50,
		humidity_point = 35,
	})

	minetest.register_biome({
		name = "grassland_ocean",
		node_top = "mapgen:sand",
		depth_top = 1,
		node_filler = "mapgen:sand",
		depth_filler = 3,
		node_riverbed = "mapgen:sand",
		depth_riverbed = 2,
		node_cave_liquid = "mapgen:water_source",
		node_dungeon = "mapgen:cobble",
		node_dungeon_alt = "mapgen:mossycobble",
		node_dungeon_stair = "stairs:stair_cobble",
		y_max = 3,
		y_min = -255,
		heat_point = 50,
		humidity_point = 35,
	})

	minetest.register_biome({
		name = "grassland_under",
		node_cave_liquid = {"mapgen:water_source", "mapgen:lava_source"},
		node_dungeon = "mapgen:cobble",
		node_dungeon_alt = "mapgen:mossycobble",
		node_dungeon_stair = "stairs:stair_cobble",
		y_max = -256,
		y_min = -31000,
		heat_point = 50,
		humidity_point = 35,
	})

	-- Coniferous forest

	minetest.register_biome({
		name = "coniferous_forest",
		node_top = "mapgen:dirt_with_coniferous_litter",
		depth_top = 1,
		node_filler = "mapgen:dirt",
		depth_filler = 3,
		node_riverbed = "mapgen:sand",
		depth_riverbed = 2,
		node_dungeon = "mapgen:cobble",
		node_dungeon_alt = "mapgen:mossycobble",
		node_dungeon_stair = "stairs:stair_cobble",
		y_max = 31000,
		y_min = 6,
		heat_point = 45,
		humidity_point = 70,
	})

	minetest.register_biome({
		name = "coniferous_forest_dunes",
		node_top = "mapgen:sand",
		depth_top = 1,
		node_filler = "mapgen:sand",
		depth_filler = 3,
		node_riverbed = "mapgen:sand",
		depth_riverbed = 2,
		node_dungeon = "mapgen:cobble",
		node_dungeon_alt = "mapgen:mossycobble",
		node_dungeon_stair = "stairs:stair_cobble",
		vertical_blend = 1,
		y_max = 5,
		y_min = 4,
		heat_point = 45,
		humidity_point = 70,
	})

	minetest.register_biome({
		name = "coniferous_forest_ocean",
		node_top = "mapgen:sand",
		depth_top = 1,
		node_filler = "mapgen:sand",
		depth_filler = 3,
		node_riverbed = "mapgen:sand",
		depth_riverbed = 2,
		node_cave_liquid = "mapgen:water_source",
		node_dungeon = "mapgen:cobble",
		node_dungeon_alt = "mapgen:mossycobble",
		node_dungeon_stair = "stairs:stair_cobble",
		y_max = 3,
		y_min = -255,
		heat_point = 45,
		humidity_point = 70,
	})

	minetest.register_biome({
		name = "coniferous_forest_under",
		node_cave_liquid = {"mapgen:water_source", "mapgen:lava_source"},
		node_dungeon = "mapgen:cobble",
		node_dungeon_alt = "mapgen:mossycobble",
		node_dungeon_stair = "stairs:stair_cobble",
		y_max = -256,
		y_min = -31000,
		heat_point = 45,
		humidity_point = 70,
	})

	-- Deciduous forest

	minetest.register_biome({
		name = "deciduous_forest",
		node_top = "mapgen:dirt_with_grass",
		depth_top = 1,
		node_filler = "mapgen:dirt",
		depth_filler = 3,
		node_riverbed = "mapgen:sand",
		depth_riverbed = 2,
		node_dungeon = "mapgen:cobble",
		node_dungeon_alt = "mapgen:mossycobble",
		node_dungeon_stair = "stairs:stair_cobble",
		y_max = 31000,
		y_min = 1,
		heat_point = 60,
		humidity_point = 68,
	})

	minetest.register_biome({
		name = "deciduous_forest_shore",
		node_top = "mapgen:dirt",
		depth_top = 1,
		node_filler = "mapgen:dirt",
		depth_filler = 3,
		node_riverbed = "mapgen:sand",
		depth_riverbed = 2,
		node_dungeon = "mapgen:cobble",
		node_dungeon_alt = "mapgen:mossycobble",
		node_dungeon_stair = "stairs:stair_cobble",
		y_max = 0,
		y_min = -1,
		heat_point = 60,
		humidity_point = 68,
	})

	minetest.register_biome({
		name = "deciduous_forest_ocean",
		node_top = "mapgen:sand",
		depth_top = 1,
		node_filler = "mapgen:sand",
		depth_filler = 3,
		node_riverbed = "mapgen:sand",
		depth_riverbed = 2,
		node_cave_liquid = "mapgen:water_source",
		node_dungeon = "mapgen:cobble",
		node_dungeon_alt = "mapgen:mossycobble",
		node_dungeon_stair = "stairs:stair_cobble",
		vertical_blend = 1,
		y_max = -2,
		y_min = -255,
		heat_point = 60,
		humidity_point = 68,
	})

	minetest.register_biome({
		name = "deciduous_forest_under",
		node_cave_liquid = {"mapgen:water_source", "mapgen:lava_source"},
		node_dungeon = "mapgen:cobble",
		node_dungeon_alt = "mapgen:mossycobble",
		node_dungeon_stair = "stairs:stair_cobble",
		y_max = -256,
		y_min = -31000,
		heat_point = 60,
		humidity_point = 68,
	})

	-- Desert

	minetest.register_biome({
		name = "desert",
		node_top = "mapgen:desert_sand",
		depth_top = 1,
		node_filler = "mapgen:desert_sand",
		depth_filler = 1,
		node_stone = "mapgen:desert_stone",
		node_riverbed = "mapgen:sand",
		depth_riverbed = 2,
		node_dungeon = "mapgen:desert_stone",
		node_dungeon_stair = "stairs:stair_desert_stone",
		y_max = 31000,
		y_min = 4,
		heat_point = 92,
		humidity_point = 16,
	})

	minetest.register_biome({
		name = "desert_ocean",
		node_top = "mapgen:sand",
		depth_top = 1,
		node_filler = "mapgen:sand",
		depth_filler = 3,
		node_stone = "mapgen:desert_stone",
		node_riverbed = "mapgen:sand",
		depth_riverbed = 2,
		node_cave_liquid = "mapgen:water_source",
		node_dungeon = "mapgen:desert_stone",
		node_dungeon_stair = "stairs:stair_desert_stone",
		vertical_blend = 1,
		y_max = 3,
		y_min = -255,
		heat_point = 92,
		humidity_point = 16,
	})

	minetest.register_biome({
		name = "desert_under",
		node_cave_liquid = {"mapgen:water_source", "mapgen:lava_source"},
		node_dungeon = "mapgen:cobble",
		node_dungeon_alt = "mapgen:mossycobble",
		node_dungeon_stair = "stairs:stair_cobble",
		y_max = -256,
		y_min = -31000,
		heat_point = 92,
		humidity_point = 16,
	})

	-- Sandstone desert

	minetest.register_biome({
		name = "sandstone_desert",
		node_top = "mapgen:sand",
		depth_top = 1,
		node_filler = "mapgen:sand",
		depth_filler = 1,
		node_stone = "mapgen:sandstone",
		node_riverbed = "mapgen:sand",
		depth_riverbed = 2,
		node_dungeon = "mapgen:sandstonebrick",
		node_dungeon_stair = "stairs:stair_sandstone_block",
		y_max = 31000,
		y_min = 4,
		heat_point = 60,
		humidity_point = 0,
	})

	minetest.register_biome({
		name = "sandstone_desert_ocean",
		node_top = "mapgen:sand",
		depth_top = 1,
		node_filler = "mapgen:sand",
		depth_filler = 3,
		node_stone = "mapgen:sandstone",
		node_riverbed = "mapgen:sand",
		depth_riverbed = 2,
		node_cave_liquid = "mapgen:water_source",
		node_dungeon = "mapgen:sandstonebrick",
		node_dungeon_stair = "stairs:stair_sandstone_block",
		y_max = 3,
		y_min = -255,
		heat_point = 60,
		humidity_point = 0,
	})

	minetest.register_biome({
		name = "sandstone_desert_under",
		node_cave_liquid = {"mapgen:water_source", "mapgen:lava_source"},
		node_dungeon = "mapgen:cobble",
		node_dungeon_alt = "mapgen:mossycobble",
		node_dungeon_stair = "stairs:stair_cobble",
		y_max = -256,
		y_min = -31000,
		heat_point = 60,
		humidity_point = 0,
	})

	-- Cold desert

	minetest.register_biome({
		name = "cold_desert",
		node_top = "mapgen:silver_sand",
		depth_top = 1,
		node_filler = "mapgen:silver_sand",
		depth_filler = 1,
		node_riverbed = "mapgen:sand",
		depth_riverbed = 2,
		node_dungeon = "mapgen:cobble",
		node_dungeon_alt = "mapgen:mossycobble",
		node_dungeon_stair = "stairs:stair_cobble",
		y_max = 31000,
		y_min = 4,
		heat_point = 40,
		humidity_point = 0,
	})

	minetest.register_biome({
		name = "cold_desert_ocean",
		node_top = "mapgen:sand",
		depth_top = 1,
		node_filler = "mapgen:sand",
		depth_filler = 3,
		node_riverbed = "mapgen:sand",
		depth_riverbed = 2,
		node_cave_liquid = "mapgen:water_source",
		node_dungeon = "mapgen:cobble",
		node_dungeon_alt = "mapgen:mossycobble",
		node_dungeon_stair = "stairs:stair_cobble",
		vertical_blend = 1,
		y_max = 3,
		y_min = -255,
		heat_point = 40,
		humidity_point = 0,
	})

	minetest.register_biome({
		name = "cold_desert_under",
		node_cave_liquid = {"mapgen:water_source", "mapgen:lava_source"},
		node_dungeon = "mapgen:cobble",
		node_dungeon_alt = "mapgen:mossycobble",
		node_dungeon_stair = "stairs:stair_cobble",
		y_max = -256,
		y_min = -31000,
		heat_point = 40,
		humidity_point = 0,
	})

	-- Savanna

	minetest.register_biome({
		name = "savanna",
		node_top = "mapgen:dry_dirt_with_dry_grass",
		depth_top = 1,
		node_filler = "mapgen:dry_dirt",
		depth_filler = 1,
		node_riverbed = "mapgen:sand",
		depth_riverbed = 2,
		node_dungeon = "mapgen:cobble",
		node_dungeon_alt = "mapgen:mossycobble",
		node_dungeon_stair = "stairs:stair_cobble",
		y_max = 31000,
		y_min = 1,
		heat_point = 89,
		humidity_point = 42,
	})

	minetest.register_biome({
		name = "savanna_shore",
		node_top = "mapgen:dry_dirt",
		depth_top = 1,
		node_filler = "mapgen:dry_dirt",
		depth_filler = 3,
		node_riverbed = "mapgen:sand",
		depth_riverbed = 2,
		node_dungeon = "mapgen:cobble",
		node_dungeon_alt = "mapgen:mossycobble",
		node_dungeon_stair = "stairs:stair_cobble",
		y_max = 0,
		y_min = -1,
		heat_point = 89,
		humidity_point = 42,
	})

	minetest.register_biome({
		name = "savanna_ocean",
		node_top = "mapgen:sand",
		depth_top = 1,
		node_filler = "mapgen:sand",
		depth_filler = 3,
		node_riverbed = "mapgen:sand",
		depth_riverbed = 2,
		node_cave_liquid = "mapgen:water_source",
		node_dungeon = "mapgen:cobble",
		node_dungeon_alt = "mapgen:mossycobble",
		node_dungeon_stair = "stairs:stair_cobble",
		vertical_blend = 1,
		y_max = -2,
		y_min = -255,
		heat_point = 89,
		humidity_point = 42,
	})

	minetest.register_biome({
		name = "savanna_under",
		node_cave_liquid = {"mapgen:water_source", "mapgen:lava_source"},
		node_dungeon = "mapgen:cobble",
		node_dungeon_alt = "mapgen:mossycobble",
		node_dungeon_stair = "stairs:stair_cobble",
		y_max = -256,
		y_min = -31000,
		heat_point = 89,
		humidity_point = 42,
	})

	-- Rainforest

	minetest.register_biome({
		name = "rainforest",
		node_top = "mapgen:dirt_with_rainforest_litter",
		depth_top = 1,
		node_filler = "mapgen:dirt",
		depth_filler = 3,
		node_riverbed = "mapgen:sand",
		depth_riverbed = 2,
		node_dungeon = "mapgen:cobble",
		node_dungeon_alt = "mapgen:mossycobble",
		node_dungeon_stair = "stairs:stair_cobble",
		y_max = 31000,
		y_min = 1,
		heat_point = 86,
		humidity_point = 65,
	})

	minetest.register_biome({
		name = "rainforest_swamp",
		node_top = "mapgen:dirt",
		depth_top = 1,
		node_filler = "mapgen:dirt",
		depth_filler = 3,
		node_riverbed = "mapgen:sand",
		depth_riverbed = 2,
		node_dungeon = "mapgen:cobble",
		node_dungeon_alt = "mapgen:mossycobble",
		node_dungeon_stair = "stairs:stair_cobble",
		y_max = 0,
		y_min = -1,
		heat_point = 86,
		humidity_point = 65,
	})

	minetest.register_biome({
		name = "rainforest_ocean",
		node_top = "mapgen:sand",
		depth_top = 1,
		node_filler = "mapgen:sand",
		depth_filler = 3,
		node_riverbed = "mapgen:sand",
		depth_riverbed = 2,
		node_cave_liquid = "mapgen:water_source",
		node_dungeon = "mapgen:cobble",
		node_dungeon_alt = "mapgen:mossycobble",
		node_dungeon_stair = "stairs:stair_cobble",
		vertical_blend = 1,
		y_max = -2,
		y_min = -255,
		heat_point = 86,
		humidity_point = 65,
	})

	minetest.register_biome({
		name = "rainforest_under",
		node_cave_liquid = {"mapgen:water_source", "mapgen:lava_source"},
		node_dungeon = "mapgen:cobble",
		node_dungeon_alt = "mapgen:mossycobble",
		node_dungeon_stair = "stairs:stair_cobble",
		y_max = -256,
		y_min = -31000,
		heat_point = 86,
		humidity_point = 65,
	})
end

minetest.clear_registered_biomes()
default.register_biomes()