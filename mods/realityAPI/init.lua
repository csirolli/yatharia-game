-- realityAPI

realityAPI = {}
local ra = realityAPI

ra.register_ore = function (modname, orename, def)
	minetest.register_node(modname..":"..orename, {
	    description = def.description,
	    tiles = {"default_stone.png^(default_mineral_tin.png^[colorize:"..def.color..")"},
	    is_ground_content = true,
	    groups = {cracky = 3},
	    drop = modname..":"..orename.."_ore"
	})
	minetest.register_craftitem(modname..":".. orename.."_ore", {
	    description = def.description.." Ore",
	    inventory_image = "default_tin_lump.png^[colorize:"..def.color,
	})
end